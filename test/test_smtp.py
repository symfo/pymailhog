import smtplib
import email.header
import email.encoders
import email.utils
import email.mime
import email.mime.text
import email.mime.multipart

import urllib.request
import json

import unittest

class TestPyMailHogBasic(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self._delete_message(self)
        self.smtpclient = smtplib.SMTP('localhost', 1025)

    @classmethod
    def tearDownClass(self):
        self.smtpclient.quit()
        #self._delete_message(self)

    def test_send_utf8(self):

        main_text = 'UTF-8本文の確認テスト🍣🍺'
        msg = email.mime.text.MIMEText(main_text, 'plain', 'utf-8')

        msg['Subject'] = 'UTF-8件名のテスト🍣🍺'
        msg['From'] = 'from@example.com'
        msg['To'] = 'to@example.com'
        msg['Cc'] = 'cc@example.com'
        msg['Date'] = email.utils.formatdate(None,True)
        msg['Message-ID'] = 'test_send_utf8'
        self._send(msg)

        rmsg = self._get_message(msg['Message-ID'])
        self.assertEqual(msg['Subject'], rmsg['subject'])
        self.assertEqual(msg['From'], rmsg['from'])
        self.assertEqual([msg['To']], rmsg['to'])
        self.assertEqual([msg['Cc']], rmsg['cc'])
        self.assertEqual(main_text, rmsg['body'])


    def test_send_sjis(self):

        main_text = 'iso-2022-jp本文の確認テスト'
        msg = email.mime.text.MIMEText(main_text, 'plain', 'iso-2022-jp')

        subject = 'SJIS件名のテスト'
        msg['Subject'] = email.header.Header(subject, 'iso-2022-jp')
        msg['From'] = 'from@example.com'
        msg['To'] = 'to@example.com'
        msg['Cc'] = 'cc@example.com'
        msg['Date'] = 'Mon, 25 Nov 2019 22:08:09 +0900'
        msg['Message-ID'] = 'test_send_sjis'
        self._send(msg)

        rmsg = self._get_message(msg['Message-ID'])
        self.assertEqual(msg['Subject'], rmsg['subject'])
        self.assertEqual(msg['From'], rmsg['from'])
        self.assertEqual([msg['To']], rmsg['to'])
        self.assertEqual([msg['Cc']], rmsg['cc'])
        self.assertEqual(main_text, rmsg['body'])
        self.assertEqual('2019/11/25 22:08:09', rmsg['date'])


    def test_sent_attachment(self):
        msg = email.mime.multipart.MIMEMultipart()

        msg['Subject'] = '添付ファイルあり'
        msg['From'] = 'from@example.com'
        msg['To'] = 'to@example.com'
        msg['Cc'] = 'cc@example.com'
        msg['Date'] = 'Mon, 25 Nov 2019 07:08:09 +0900'
        msg['Message-ID'] = 'test_sent_attachment'

        main_text = '添付ファイルの送信テスト(UTF8)'
        msg.attach(email.mime.text.MIMEText(main_text, 'plain', 'utf-8'))


        attachment = email.mime.base.MIMEBase('application', 'octet-stream')
        attachment.set_payload('添付ファイルテスト'.encode('utf-8'))
        email.encoders.encode_base64(attachment)
        attachment.add_header('Content-Disposition', 'attachment; filename="attachment.txt"')
        msg.attach(attachment)

        self._send(msg)

        rmsg = self._get_message(msg['Message-ID'])
        self.assertEqual(msg['Subject'], rmsg['subject'])
        self.assertEqual(msg['From'], rmsg['from'])
        self.assertEqual([msg['To']], rmsg['to'])
        self.assertEqual([msg['Cc']], rmsg['cc'])
        self.assertEqual(main_text, rmsg['body'])
        self.assertEqual('2019/11/25 07:08:09', rmsg['date'])

        attachments = rmsg['attachments'][0]
        self.assertEqual('attachment.txt', attachments['filename'])
        

    def test_send_long_message(self):

        main_text = 'UTF-8本文が長いメールの送信\n'
        main_text += 'テストテキスト' * 30
        main_text += 'testtext' * 30
        main_text += '\n' * 3
        main_text += 'テストテキスト\n' * 30
        msg = email.mime.text.MIMEText(main_text, 'plain', 'utf-8')

        msg['Subject'] = 'UTF-8本文メッセージ折返し🍣🍺'
        msg['From'] = 'from@example.com'
        msg['To'] = 'to@example.com'
        msg['Cc'] = 'cc@example.com'
        msg['Date'] = email.utils.formatdate(None,True)
        msg['Message-ID'] = 'test_send_long_message_utf8'
        self._send(msg)

        rmsg = self._get_message(msg['Message-ID'])
        self.assertEqual(msg['Subject'], rmsg['subject'])
        self.assertEqual(msg['From'], rmsg['from'])
        self.assertEqual([msg['To']], rmsg['to'])
        self.assertEqual([msg['Cc']], rmsg['cc'])
        self.assertEqual(main_text, rmsg['body'])

    def test_send_2_mails(self):
        main_text1 = '1通目のメール本文'
        msg1 = email.mime.text.MIMEText(main_text1, 'plain', 'utf-8')

        msg1['Subject'] = '1通目のメール'
        msg1['From'] = 'from@example.com'
        msg1['To'] = 'to2@example.com'
        msg1['Cc'] = 'cc2@example.com'
        msg1['Date'] = email.utils.formatdate(None,True)
        msg1['Message-ID'] = 'mail-1'
        rmsg1 = self._send(msg1)

        main_text2 = '2通目のメール本文'
        msg2 = email.mime.text.MIMEText(main_text2, 'plain', 'utf-8')

        msg2['Subject'] = '2通目のメール'
        msg2['From'] = 'from@example.com'
        msg2['To'] = 'to2@example.com'
        msg2['Cc'] = 'cc2@example.com'
        msg2['Date'] = email.utils.formatdate(None,True)
        msg2['Message-ID'] = 'mail-2'
        rmsg1 = self._send(msg2)

        rmsg1 = self._get_message(msg1['Message-ID'])
        self.assertEqual(msg1['Subject'], rmsg1['subject'])
        self.assertEqual(msg1['From'], rmsg1['from'])
        self.assertEqual([msg1['To']], rmsg1['to'])
        self.assertEqual([msg1['Cc']], rmsg1['cc'])
        self.assertEqual(main_text1, rmsg1['body'])

        rmsg2 = self._get_message(msg2['Message-ID'])
        self.assertEqual(msg2['Subject'], rmsg2['subject'])
        self.assertEqual(msg2['From'], rmsg2['from'])
        self.assertEqual([msg2['To']], rmsg2['to'])
        self.assertEqual([msg2['Cc']], rmsg2['cc'])
        self.assertEqual(main_text2, rmsg2['body'])


    def _get_message(self, msg_id=None):
        url = 'http://localhost:8025/api/messages'
        if msg_id:
            url += '/'+msg_id
        response = urllib.request.urlopen(url)
        body = response.read().decode('utf-8')
        return json.loads(body)

    def _delete_message(self, msg_id=None):
        url = 'http://localhost:8025/api/messages'
        if msg_id:
            url += '/' + msg_id
        req = urllib.request.Request(url, method='DELETE')
        urllib.request.urlopen(req)

    def _send(self, msg):
        self.smtpclient.send_message(msg)
        

if __name__ == '__main__':
    unittest.main()